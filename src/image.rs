use image as i;
use std::path::Path;

pub struct Image {
  pub image: i::DynamicImage,
  pub rgb: i::RgbImage,
  pub width: u32,
  pub height: u32,
  pub row_pitch: usize,
  pub stride: usize,
  pub size: usize
}

impl Image {
  fn new(image: i::DynamicImage) -> Image {
    let rgb = image.to_rgb();
    let (width, height) = rgb.dimensions();
    let width = width;
    let height = height;
    let stride = 4usize;
    let row_pitch = stride * width as usize;
    let size = stride * width as usize * height as usize;
    Image {
      image,
      rgb,
      width,
      height,
      row_pitch,
      stride,
      size
    }
  }
  pub fn find(path: &str, fallback_data: &[u8]) -> Image {
    let target_path = cargo_bundle::resource_path(path);
    if let Some(target_path) = target_path {
      Image::new(i::open(&target_path).unwrap())
    } else {
      let file = Path::new(&path);
      if file.exists() {
        Image::new(i::open(&file).unwrap())
      } else {
        let file = match nfd2::open_file_dialog(Some("png,jpg,gif,bmp,tiff,tga"), None)
          .unwrap_or_else(|e| {
            println!("{:?}", e);
            panic!(e);
          }) {
          nfd2::Response::Okay(file) => Some(file),
          nfd2::Response::OkayMultiple(files) => Some(files[0].clone()), // Never reach
          nfd2::Response::Cancel => None
        };
        match file {
          Some(file) => Image::new(i::open(&file).unwrap()),
          None => {
            Image::new(i::load(std::io::Cursor::new(fallback_data), i::ImageFormat::Png).unwrap())
          }
        }
      }
    }
  }
}