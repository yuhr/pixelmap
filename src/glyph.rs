pub struct Glyph {
  pub texture: wgpu::Texture,
  pub view: wgpu::TextureView,
  pub extent: wgpu::Extent3d,
  pub row_pitch: usize,
}

impl Glyph {
  pub fn new(device: &wgpu::Device, width: u32, height: u32) -> Glyph {
    let extent = wgpu::Extent3d {
      width,
      height,
      depth: 1,
    };
    let texture = device.create_texture(&wgpu::TextureDescriptor {
      label: None,
      size: extent,
      mip_level_count: 1,
      sample_count: 1,
      dimension: wgpu::TextureDimension::D2,
      format: wgpu::TextureFormat::R8Unorm,
      usage: wgpu::TextureUsage::SAMPLED | wgpu::TextureUsage::COPY_DST,
    });
    let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
    Glyph {
      texture,
      view,
      extent,
      row_pitch: extent.width as usize,
    }
  }
  pub fn write(&self, queue: &wgpu::Queue, data: &[u8]) {
    queue.write_texture(
      wgpu::TextureCopyView {
        texture: &self.texture,
        mip_level: 0,
        origin: wgpu::Origin3d::ZERO,
      },
      data,
      wgpu::TextureDataLayout {
        offset: 0,
        bytes_per_row: self.row_pitch as u32,
        rows_per_image: 0,
      },
      self.extent,
    );
  }
}