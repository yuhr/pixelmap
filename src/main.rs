const VERSION: &'static str = env!("CARGO_PKG_VERSION");

use winit::{
  event::{Event, WindowEvent},
  event_loop::{ControlFlow, EventLoop},
  window::Window
};

use bytemuck::{Pod, Zeroable};
use lazy_static::lazy_static;
use rug::{
  ops::{DivFrom, NegAssign, Pow},
  Assign, Float, Integer
};
use std::cmp::PartialEq;
use std::collections::HashMap;
use std::time::{Duration, Instant};
use wgpu::util::DeviceExt;

mod glyph;
mod glyph_caches;
mod image;
mod texture;

#[repr(C)]
#[derive(Clone, Copy, Pod, Zeroable)]
struct Vertex {
  pos: [f32; 2],
  tex_coord: [f32; 2]
}

#[repr(C)]
#[derive(Clone, Copy, Pod, Zeroable)]
struct UniformBuffer {
  viewport: [f32; 2],
  is_scaled: u32
}

#[repr(C)]
#[derive(Clone, Copy, Pod, Zeroable)]
struct StorageBuffer {
  viewport: [f32; 2],
  position: [f32; 2],
  size: [f32; 2],
  is_scaled: u32,
  dummy_0: u32
}

const fn vertex(pos: [i8; 2], tc: [i8; 2]) -> Vertex {
  Vertex {
    pos: [pos[0] as f32, pos[1] as f32],
    tex_coord: [tc[0] as f32, tc[1] as f32]
  }
}

fn create_vertices() -> (Vec<Vertex>, Vec<u16>) {
  let vertex_data = [
    vertex([-1, 1], [0, 0]),
    vertex([1, 1], [1, 0]),
    vertex([-1, -1], [0, 1]),
    vertex([1, -1], [1, 1])
  ];

  let index_data: &[u16] = &[0, 1, 2, 3];

  (vertex_data.to_vec(), index_data.to_vec())
}

#[derive(PartialEq)]
enum State {
  Finished,
  Forward,
  Play,
  Backward
}

struct Renderer {
  window_ids: HashMap<winit::window::WindowId, u8>,
  window: Window,
  subwindow: Window,
  instance: wgpu::Instance,
  surface: wgpu::Surface,
  sub_surface: wgpu::Surface,
  adapter: wgpu::Adapter,
  device: wgpu::Device,
  queue: wgpu::Queue,
  vs_module: wgpu::ShaderModule,
  fs_module: wgpu::ShaderModule,
  sub_vs_module: wgpu::ShaderModule,
  sub_fs_module: wgpu::ShaderModule,
  pipeline_layout: wgpu::PipelineLayout,
  sub_pipeline_layout: wgpu::PipelineLayout,
  render_pipeline: wgpu::RenderPipeline,
  sub_render_pipeline: wgpu::RenderPipeline,
  swapchain: wgpu::SwapChain,
  swapchain_desc: wgpu::SwapChainDescriptor,
  subswapchain: wgpu::SwapChain,
  subswapchain_desc: wgpu::SwapChainDescriptor,
  bind_group_layout: wgpu::BindGroupLayout,
  bind_group: wgpu::BindGroup,
  end_bind_group: wgpu::BindGroup,
  vertex_buffer: wgpu::Buffer,
  index_buffer: wgpu::Buffer,
  uniform_buffer: wgpu::Buffer,
  storage_buffer: wgpu::Buffer,
  texture: texture::Texture,
  end_texture: texture::Texture,
  sampler: wgpu::Sampler,
  uniform_data: UniformBuffer,
  storage_data: [StorageBuffer; 1024],
  clock: Instant,
  start_time: Instant,
  last_second_time: Instant,
  title: String,
  state: State,
  is_paused: bool,
  velocity: Integer,
  current_frame: Integer,
  initial_frame: Integer,
  first_frame: Integer,
  final_frame: Integer,
  texels_rgba: Vec<u8>,
  texels_rgb: Vec<u8>,
  start_image: image::Image,
  end_image: image::Image,
  glyph_caches: glyph_caches::GlyphCaches,
  clock_history: Vec<u32>,
  elapsed_frames: Integer,
  fps: Float,
  elapsed_secs: Float,
  elapsed_mins: Integer,
  elapsed_hours: Integer,
  elapsed_days: Integer,
  elapsed_years: Integer
}

impl Renderer {
  pub async fn new(
    window: Window,
    swapchain_format: wgpu::TextureFormat,
    subwindow: Window
  ) -> Renderer {
    let font = include_bytes!("data/sarasa-monoslab-j-regular.ttf") as &[u8];
    let font = fontdue::Font::from_bytes(font, fontdue::FontSettings::default()).unwrap();

    let size = window.inner_size();
    let instance = wgpu::Instance::new(wgpu::BackendBit::PRIMARY);
    let surface = unsafe { instance.create_surface(&window) };
    let sub_surface = unsafe { instance.create_surface(&subwindow) };
    let adapter = instance
      .request_adapter(&wgpu::RequestAdapterOptions {
        power_preference: wgpu::PowerPreference::HighPerformance,
        // Request an adapter which can render to our surface
        compatible_surface: Some(&surface)
      })
      .await
      .expect("Failed to find an appropiate adapter");
    println!(
      "{}, {:?}",
      &adapter.get_info().name,
      &adapter.get_info().backend
    );

    // Create the logical device and command queue
    let (device, queue) = adapter
      .request_device(
        &wgpu::DeviceDescriptor {
          features: wgpu::Features::empty(),
          limits: wgpu::Limits::default(),
          shader_validation: true
        },
        None
      )
      .await
      .expect("Failed to create device");

    let (vertex_data, index_data) = create_vertices();
    let vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
      label: Some("Vertex Buffer"),
      contents: bytemuck::cast_slice(&vertex_data),
      usage: wgpu::BufferUsage::VERTEX
    });
    let index_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
      label: Some("Index Buffer"),
      contents: bytemuck::cast_slice(&index_data),
      usage: wgpu::BufferUsage::INDEX
    });

    let bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
      label: None,
      entries: &[
        wgpu::BindGroupLayoutEntry {
          binding: 0,
          visibility: wgpu::ShaderStage::VERTEX,
          ty: wgpu::BindingType::UniformBuffer {
            dynamic: false,
            min_binding_size: wgpu::BufferSize::new(std::mem::size_of::<UniformBuffer>() as u64)
          },
          count: None
        },
        wgpu::BindGroupLayoutEntry {
          binding: 1,
          visibility: wgpu::ShaderStage::FRAGMENT,
          ty: wgpu::BindingType::SampledTexture {
            multisampled: false,
            component_type: wgpu::TextureComponentType::Float,
            dimension: wgpu::TextureViewDimension::D2
          },
          count: None
        },
        wgpu::BindGroupLayoutEntry {
          binding: 2,
          visibility: wgpu::ShaderStage::FRAGMENT,
          ty: wgpu::BindingType::Sampler { comparison: false },
          count: None
        }
      ]
    });
    let sub_bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
      label: None,
      entries: &[
        wgpu::BindGroupLayoutEntry {
          binding: 0,
          visibility: wgpu::ShaderStage::VERTEX,
          ty: wgpu::BindingType::StorageBuffer {
            dynamic: false,
            readonly: true,
            min_binding_size: wgpu::BufferSize::new(std::mem::size_of::<StorageBuffer>() as u64)
          },
          count: None
        },
        wgpu::BindGroupLayoutEntry {
          binding: 1,
          visibility: wgpu::ShaderStage::FRAGMENT,
          ty: wgpu::BindingType::SampledTexture {
            multisampled: false,
            component_type: wgpu::TextureComponentType::Float,
            dimension: wgpu::TextureViewDimension::D2
          },
          count: None
        },
        wgpu::BindGroupLayoutEntry {
          binding: 2,
          visibility: wgpu::ShaderStage::FRAGMENT,
          ty: wgpu::BindingType::Sampler { comparison: false },
          count: None
        }
      ]
    });

    let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
      label: None,
      bind_group_layouts: &[&bind_group_layout],
      push_constant_ranges: &[]
    });
    let sub_pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
      label: None,
      bind_group_layouts: &[&sub_bind_group_layout],
      push_constant_ranges: &[]
    });

    // Create the textures
    let start_image = image::Image::find("start.png", &include_bytes!("data/start.png")[..]);

    let texels_rgb = (&(*start_image.rgb)).to_vec();
    let current_frame = Integer::from_digits::<u8>(&texels_rgb, rug::integer::Order::LsfLe);
    let initial_frame = current_frame.clone();
    let first_frame = Integer::from(0);
    let final_frame = Integer::from_digits::<u8>(
      &vec![0xFFu8; 3usize * start_image.width as usize * start_image.height as usize],
      rug::integer::Order::LsfLe
    );
    let velocity = Integer::from(1);
    let mut texels_rgba = Vec::with_capacity(start_image.size);
    for i in 0..texels_rgb.len() {
      texels_rgba.push(texels_rgb[i]);
      if i % 3 == 2 {
        texels_rgba.push(0xFFu8);
      };
    }

    let texture = texture::Texture::new(&device, start_image.width, start_image.height);
    texture.write(&queue, &texels_rgba);

    let end_image = image::Image::find("end.png", &include_bytes!("data/end.png")[..]);
    let end_texture = {
      let texels_rgb = (&(*end_image.rgb)).to_vec();
      let mut texels_rgba = Vec::with_capacity(end_image.size);
      for i in 0..texels_rgb.len() {
        texels_rgba.push(texels_rgb[i]);
        if i % 3 == 2 {
          texels_rgba.push(0xFFu8);
        };
      }
      let end_texture = texture::Texture::new(&device, end_image.width, end_image.height);
      end_texture.write(&queue, &texels_rgba);
      end_texture
    };

    // Create other resources
    let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
      address_mode_u: wgpu::AddressMode::ClampToEdge,
      address_mode_v: wgpu::AddressMode::ClampToEdge,
      address_mode_w: wgpu::AddressMode::ClampToEdge,
      mag_filter: wgpu::FilterMode::Nearest,
      min_filter: wgpu::FilterMode::Linear,
      mipmap_filter: wgpu::FilterMode::Nearest,
      ..Default::default()
    });

    let uniform_data = [UniformBuffer {
      viewport: [size.width as f32, size.height as f32],
      is_scaled: 1
    }];
    let uniform_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
      label: Some("Uniform Buffer"),
      contents: bytemuck::cast_slice(&uniform_data),
      usage: wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST
    });

    let storage_data = [StorageBuffer {
      viewport: [size.width as f32, size.height as f32],
      position: [0.0, 0.0],
      size: [720.0, 480.0],
      is_scaled: 0,
      dummy_0: 0
    }; 1024];
    let storage_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
      label: Some("Storage Buffer"),
      contents: bytemuck::cast_slice(&storage_data),
      usage: wgpu::BufferUsage::STORAGE | wgpu::BufferUsage::COPY_DST
    });

    let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
      layout: &bind_group_layout,
      entries: &[
        wgpu::BindGroupEntry {
          binding: 0,
          resource: wgpu::BindingResource::Buffer(uniform_buffer.slice(..))
        },
        wgpu::BindGroupEntry {
          binding: 1,
          resource: wgpu::BindingResource::TextureView(&texture.view)
        },
        wgpu::BindGroupEntry {
          binding: 2,
          resource: wgpu::BindingResource::Sampler(&sampler)
        }
      ],
      label: None
    });
    let end_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
      layout: &bind_group_layout,
      entries: &[
        wgpu::BindGroupEntry {
          binding: 0,
          resource: wgpu::BindingResource::Buffer(uniform_buffer.slice(..))
        },
        wgpu::BindGroupEntry {
          binding: 1,
          resource: wgpu::BindingResource::TextureView(&end_texture.view)
        },
        wgpu::BindGroupEntry {
          binding: 2,
          resource: wgpu::BindingResource::Sampler(&sampler)
        }
      ],
      label: None
    });

    let vertex_size = std::mem::size_of::<Vertex>();
    let vertex_state = wgpu::VertexStateDescriptor {
      index_format: wgpu::IndexFormat::Uint16,
      vertex_buffers: &[wgpu::VertexBufferDescriptor {
        stride: vertex_size as wgpu::BufferAddress,
        step_mode: wgpu::InputStepMode::Vertex,
        attributes: &[
          wgpu::VertexAttributeDescriptor {
            format: wgpu::VertexFormat::Float2,
            offset: 0,
            shader_location: 0
          },
          wgpu::VertexAttributeDescriptor {
            format: wgpu::VertexFormat::Float2,
            offset: 4 * 2,
            shader_location: 1
          }
        ]
      }]
    };

    // Compile the shaders
    let mut compiler = shaderc::Compiler::new().unwrap();
    let options = shaderc::CompileOptions::new().unwrap();

    let shader_vert = compiler
      .compile_into_spirv(
        &include_str!("data/shader.vert"),
        shaderc::ShaderKind::Vertex,
        "shader.glsl",
        "main",
        Some(&options)
      )
      .unwrap();
    let sub_shader_vert = compiler
      .compile_into_spirv(
        &include_str!("data/subshader.vert"),
        shaderc::ShaderKind::Vertex,
        "shader.glsl",
        "main",
        Some(&options)
      )
      .unwrap();
    let shader_frag = compiler
      .compile_into_spirv(
        &include_str!("data/shader.frag"),
        shaderc::ShaderKind::Fragment,
        "shader.glsl",
        "main",
        Some(&options)
      )
      .unwrap();
    let sub_shader_frag = compiler
      .compile_into_spirv(
        &include_str!("data/subshader.frag"),
        shaderc::ShaderKind::Fragment,
        "shader.glsl",
        "main",
        Some(&options)
      )
      .unwrap();
    let vs_module = device.create_shader_module(wgpu::util::make_spirv(shader_vert.as_binary_u8()));
    let fs_module = device.create_shader_module(wgpu::util::make_spirv(shader_frag.as_binary_u8()));
    let sub_vs_module =
      device.create_shader_module(wgpu::util::make_spirv(sub_shader_vert.as_binary_u8()));
    let sub_fs_module =
      device.create_shader_module(wgpu::util::make_spirv(sub_shader_frag.as_binary_u8()));

    let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
      label: None,
      layout: Some(&pipeline_layout),
      vertex_stage: wgpu::ProgrammableStageDescriptor {
        module: &vs_module,
        entry_point: "main"
      },
      fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
        module: &fs_module,
        entry_point: "main"
      }),
      // Use the default rasterizer state: no culling, no depth bias
      rasterization_state: Some(wgpu::RasterizationStateDescriptor {
        front_face: wgpu::FrontFace::Cw,
        cull_mode: wgpu::CullMode::Back,
        ..Default::default()
      }),
      primitive_topology: wgpu::PrimitiveTopology::TriangleStrip,
      color_states: &[wgpu::ColorStateDescriptor {
        format: swapchain_format,
        color_blend: wgpu::BlendDescriptor::REPLACE,
        alpha_blend: wgpu::BlendDescriptor::REPLACE,
        write_mask: wgpu::ColorWrite::ALL
      }],
      depth_stencil_state: None,
      vertex_state: vertex_state.clone(),
      sample_count: 1,
      sample_mask: !0,
      alpha_to_coverage_enabled: false
    });
    let sub_render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
      label: None,
      layout: Some(&sub_pipeline_layout),
      vertex_stage: wgpu::ProgrammableStageDescriptor {
        module: &sub_vs_module,
        entry_point: "main"
      },
      fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
        module: &sub_fs_module,
        entry_point: "main"
      }),
      // Use the default rasterizer state: no culling, no depth bias
      rasterization_state: Some(wgpu::RasterizationStateDescriptor {
        front_face: wgpu::FrontFace::Cw,
        cull_mode: wgpu::CullMode::Back,
        ..Default::default()
      }),
      primitive_topology: wgpu::PrimitiveTopology::TriangleStrip,
      color_states: &[wgpu::ColorStateDescriptor {
        format: swapchain_format,
        color_blend: wgpu::BlendDescriptor {
          src_factor: wgpu::BlendFactor::SrcAlpha,
          dst_factor: wgpu::BlendFactor::OneMinusSrcAlpha,
          operation: wgpu::BlendOperation::Add
        },
        alpha_blend: wgpu::BlendDescriptor {
          src_factor: wgpu::BlendFactor::One,
          dst_factor: wgpu::BlendFactor::OneMinusSrcAlpha,
          operation: wgpu::BlendOperation::Add
        },
        write_mask: wgpu::ColorWrite::ALL
      }],
      depth_stencil_state: None,
      vertex_state: vertex_state.clone(),
      sample_count: 1,
      sample_mask: !0,
      alpha_to_coverage_enabled: true
    });

    let swapchain_desc = wgpu::SwapChainDescriptor {
      usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
      format: swapchain_format,
      width: size.width,
      height: size.height,
      present_mode: if cfg!(debug_assertions) {
        wgpu::PresentMode::Immediate
      } else {
        wgpu::PresentMode::Mailbox
      }
    };
    let subswapchain_desc = wgpu::SwapChainDescriptor {
      usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
      format: swapchain_format,
      width: size.width,
      height: size.height,
      present_mode: if cfg!(debug_assertions) {
        wgpu::PresentMode::Immediate
      } else {
        wgpu::PresentMode::Mailbox
      }
    };

    let swapchain = device.create_swap_chain(&surface, &swapchain_desc);
    let subswapchain = device.create_swap_chain(&sub_surface, &subswapchain_desc);

    let mut window_ids = HashMap::new();
    window_ids.insert(window.id(), 0);
    window_ids.insert(subwindow.id(), 1);

    let mut glyph_caches = glyph_caches::GlyphCaches::new(font, sub_bind_group_layout);
    glyph_caches.populate(
      &device,
      &queue,
      &"+-01234567890.⁺⁻⁰¹²³⁴⁵⁶⁷⁸⁹:;/yearsdminhouc",
      &storage_buffer,
      &sampler
    );

    Renderer {
      window_ids,
      window,
      subwindow,
      instance,
      surface,
      sub_surface,
      adapter,
      device,
      queue,
      vs_module,
      fs_module,
      sub_vs_module,
      sub_fs_module,
      pipeline_layout,
      sub_pipeline_layout,
      render_pipeline,
      sub_render_pipeline,
      swapchain,
      swapchain_desc,
      subswapchain,
      subswapchain_desc,
      bind_group_layout,
      bind_group,
      end_bind_group,
      vertex_buffer,
      index_buffer,
      uniform_buffer,
      storage_buffer,
      texture,
      end_texture,
      sampler,
      uniform_data: uniform_data[0],
      storage_data,
      clock: Instant::now(),
      start_time: Instant::now(),
      last_second_time: Instant::now(),
      title: format!("v{}", VERSION).to_string(),
      state: State::Play,
      is_paused: false,
      current_frame,
      initial_frame,
      first_frame,
      final_frame,
      velocity,
      texels_rgba,
      texels_rgb,
      start_image,
      end_image,
      glyph_caches,
      clock_history: Vec::with_capacity(60),
      elapsed_frames: Integer::from(0),
      fps: Float::with_val(64, 29.97),
      elapsed_secs: Float::with_val(64, 29.97),
      elapsed_mins: Integer::from(0),
      elapsed_hours: Integer::from(0),
      elapsed_days: Integer::from(0),
      elapsed_years: Integer::from(0)
    }
  }
}

const FRAMES_PER_10_MINUTES: u32 = 6 * 29_97;
const FRAMES_PER_HOUR: u32 = 36 * 29_97;
const FRAMES_PER_DAY: u32 = 86_4 * 29_97;
const FRAMES_PER_YEAR: u32 = 31_557_6 * 29_97; // Julian year

lazy_static! {
  static ref FRAMES_FOR_10_MINUTES: Integer = Integer::from(FRAMES_PER_10_MINUTES);
  static ref FRAMES_FOR_HOUR: Integer = Integer::from(FRAMES_PER_HOUR);
  static ref FRAMES_FOR_DAY: Integer = Integer::from(FRAMES_PER_DAY);
  static ref FRAMES_FOR_YEAR: Integer = Integer::from(FRAMES_PER_YEAR);
  static ref FRAMES_FOR_KEY1: Integer = Integer::from(&*FRAMES_FOR_DAY);
  static ref FRAMES_FOR_KEY2: Integer = Integer::from(&*FRAMES_FOR_YEAR);
  static ref FRAMES_FOR_KEY3: Integer = Integer::from(&*FRAMES_FOR_YEAR * 10);
  static ref FRAMES_FOR_KEY4: Integer = Integer::from(&*FRAMES_FOR_YEAR * 100);
  static ref FRAMES_FOR_KEY5: Integer = Integer::from(&*FRAMES_FOR_YEAR * 1_000);
  static ref FRAMES_FOR_KEY6: Integer = Integer::from(&*FRAMES_FOR_YEAR * 10_000);
  static ref FRAMES_FOR_KEY7: Integer = Integer::from(&*FRAMES_FOR_YEAR * 1_000_000);
  static ref FRAMES_FOR_KEY8: Integer = Integer::from(&*FRAMES_FOR_YEAR * 10_000_000_000u64);
  static ref FRAMES_FOR_KEY9: Integer =
    Integer::from(&*FRAMES_FOR_YEAR * &Integer::from(10).pow(10_000));
  static ref FRAMES_FOR_KEY0: Integer =
    Integer::from(&*FRAMES_FOR_YEAR * &Integer::from(10).pow(100_000));
  static ref FRAMES_FOR_MINUS: Integer =
    Integer::from(&*FRAMES_FOR_YEAR * &Integer::from(10).pow(1_000_000));
}

fn skip_frames(velocity: &mut Integer, skipped_frames: &Integer, is_backward: bool) {
  velocity.assign(skipped_frames);
  if is_backward {
    velocity.neg_assign();
  }
}

fn to_sup(numbers: &str) -> String {
  numbers
    .chars()
    .map(|digit| match digit {
      '+' => '⁺',
      '-' => '⁻',
      '0' => '⁰',
      '1' => '¹',
      '2' => '²',
      '3' => '³',
      '4' => '⁴',
      '5' => '⁵',
      '6' => '⁶',
      '7' => '⁷',
      '8' => '⁸',
      '9' => '⁹',
      c => c
    })
    .collect::<String>()
}

async fn run(
  event_loop: EventLoop<()>,
  window: Window,
  swapchain_format: wgpu::TextureFormat,
  subwindow: Window
) {
  let mut renderer = Renderer::new(window, swapchain_format, subwindow).await;
  event_loop.run(move |event, _, control_flow| {
    *control_flow = ControlFlow::Poll;
    use winit::event::ElementState::{Pressed, Released};
    use winit::event::VirtualKeyCode::*;
    match event {
      Event::MainEventsCleared => {
        renderer.window.request_redraw();
      }
      Event::WindowEvent { event, window_id } => match event {
        WindowEvent::CloseRequested => {
          *control_flow = ControlFlow::Exit;
        }
        WindowEvent::KeyboardInput {
          input:
            winit::event::KeyboardInput {
              virtual_keycode: Some(virtual_keycode),
              modifiers,
              state,
              ..
            },
          ..
        } => match (virtual_keycode, state) {
          (Escape, Pressed) => {
            *control_flow = ControlFlow::Exit;
          }
          (Q, Pressed) => {
            *control_flow = ControlFlow::Exit;
          }
          (F, Pressed) => {
            if renderer.window.fullscreen().is_some() {
              renderer.window.set_fullscreen(None);
            } else {
              renderer
                .window
                .set_fullscreen(Some(winit::window::Fullscreen::Borderless(
                  renderer.window.current_monitor()
                )));
            }
          }
          (S, Pressed) => {
            renderer.uniform_data.is_scaled = !renderer.uniform_data.is_scaled;
            renderer.queue.write_buffer(
              &renderer.uniform_buffer,
              0,
              bytemuck::cast_slice(&[renderer.uniform_data])
            );
          }
          (Z, Pressed) => {
            renderer.current_frame.clone_from(&renderer.first_frame);
          }
          (E, Pressed) => {
            renderer.current_frame.clone_from(&renderer.final_frame);
          }
          (R, Pressed) => {
            renderer.current_frame.clone_from(&renderer.initial_frame);
          }
          (P, Pressed) => {
            renderer.is_paused = !renderer.is_paused;
          }
          (Space, Pressed) => match &renderer.state {
            State::Finished => {
              renderer.current_frame.clone_from(&renderer.initial_frame);
              renderer.is_paused = false;
            }
            _ => {
              renderer.is_paused = !renderer.is_paused;
            }
          },
          (Right, Pressed) => {
            renderer.state = State::Forward;
          }
          (Right, Released) => {
            renderer.state = State::Play;
          }
          (Left, Pressed) => {
            renderer.state = State::Backward;
          }
          (Left, Released) => {
            renderer.state = State::Play;
          }
          (Key1, Pressed) => {
            skip_frames(&mut renderer.velocity, &*FRAMES_FOR_KEY1, modifiers.shift());
          }
          (Key2, Pressed) => {
            skip_frames(&mut renderer.velocity, &*FRAMES_FOR_KEY2, modifiers.shift());
          }
          (Key3, Pressed) => {
            skip_frames(&mut renderer.velocity, &*FRAMES_FOR_KEY3, modifiers.shift());
          }
          (Key4, Pressed) => {
            skip_frames(&mut renderer.velocity, &*FRAMES_FOR_KEY4, modifiers.shift());
          }
          (Key5, Pressed) => {
            skip_frames(&mut renderer.velocity, &*FRAMES_FOR_KEY5, modifiers.shift());
          }
          (Key6, Pressed) => {
            skip_frames(&mut renderer.velocity, &*FRAMES_FOR_KEY6, modifiers.shift());
          }
          (Key7, Pressed) => {
            skip_frames(&mut renderer.velocity, &*FRAMES_FOR_KEY7, modifiers.shift());
          }
          (Key8, Pressed) => {
            skip_frames(&mut renderer.velocity, &*FRAMES_FOR_KEY8, modifiers.shift());
          }
          (Key9, Pressed) => {
            skip_frames(&mut renderer.velocity, &*FRAMES_FOR_KEY9, modifiers.shift());
          }
          (Key0, Pressed) => {
            skip_frames(&mut renderer.velocity, &*FRAMES_FOR_KEY0, modifiers.shift());
          }
          (Minus, Pressed) => {
            skip_frames(
              &mut renderer.velocity,
              &*FRAMES_FOR_MINUS,
              modifiers.shift()
            );
          }
          (_, Pressed) => {
            println!(
              "Uncaught key input detected at window {}: {}",
              renderer.window_ids[&window_id], virtual_keycode as u32
            );
          }
          _ => {}
        },
        WindowEvent::Resized(size) => {
          match renderer.window_ids[&window_id] {
            0 => {
              // Recreate the swap chain with the new size
              renderer.swapchain_desc.width = size.width;
              renderer.swapchain_desc.height = size.height;
              renderer.swapchain = renderer
                .device
                .create_swap_chain(&renderer.surface, &renderer.swapchain_desc);

              // Update the uniform buffer
              renderer.uniform_data.viewport = [size.width as f32, size.height as f32];
              renderer.queue.write_buffer(
                &renderer.uniform_buffer,
                0,
                bytemuck::cast_slice(&[renderer.uniform_data])
              );
              renderer
                .window
                .set_cursor_visible(renderer.window.fullscreen().is_none())
            }
            1 => {}
            _ => {}
          }
        }
        _ => {}
      },
      Event::RedrawRequested(_) => {
        let now = Instant::now();

        let is_await = if cfg!(debug_assertions) {
          true
        } else {
          (1.0 / 29.97) <= (now.duration_since(renderer.clock)).as_secs_f64()
        };
        if is_await {
          if 1000 <= (now.duration_since(renderer.last_second_time)).as_millis() {
            renderer.title = format!(
              "v{} - fps: {:.3}",
              VERSION,
              ((1.0 / (now.duration_since(renderer.clock)).as_secs_f64()) * 1000.0).round()
                / 1000.0
            );
            renderer.last_second_time = renderer
              .last_second_time
              .checked_add(Duration::from_millis(1000))
              .unwrap();
            renderer.window.set_title(&renderer.title);
          }
          renderer.clock = now;

          {
            renderer.current_frame += &renderer.velocity;
            renderer
              .current_frame
              .clamp_mut(&renderer.first_frame, &renderer.final_frame);

            if renderer.is_paused {
              renderer.velocity.assign(0);
            } else {
              match renderer.state {
                State::Finished => {}
                State::Forward => {
                  if renderer.velocity <= 0 {
                    renderer.velocity.assign(1);
                  }
                  renderer.velocity *= 10;
                }
                State::Play => {
                  renderer.velocity.assign(1);
                }
                State::Backward => {
                  if 0 <= renderer.velocity {
                    renderer.velocity.assign(-1);
                  }
                  renderer.velocity *= 10;
                }
              }
            }

            match &renderer.state {
              State::Forward | State::Backward => {}
              _ => {
                if renderer.current_frame == renderer.final_frame {
                  renderer.state = State::Finished;
                } else {
                  renderer.state = State::Play;
                }
              }
            }

            unsafe {
              renderer.current_frame.write_digits_unaligned::<u8>(
                renderer.texels_rgb.as_mut_ptr(),
                renderer.texels_rgb.len(),
                rug::integer::Order::LsfLe
              );
            }

            for i in 0..renderer.texels_rgba.len() {
              let j = i / 4;
              let k = i % 4;
              if k == 3 {
                continue;
              }
              let l = j * 3 + k;
              renderer.texels_rgba[i] = renderer.texels_rgb[l];
            }

            renderer.queue.write_texture(
              wgpu::TextureCopyView {
                texture: &renderer.texture.texture,
                mip_level: 0,
                origin: wgpu::Origin3d::ZERO
              },
              &renderer.texels_rgba,
              wgpu::TextureDataLayout {
                offset: 0,
                bytes_per_row: renderer.start_image.row_pitch as u32,
                rows_per_image: 0
              },
              renderer.texture.extent
            );
          }

          let current_frame = renderer
            .swapchain
            .get_current_frame()
            .expect("Failed to acquire next swap chain texture")
            .output;

          let current_frame_sub = renderer
            .subswapchain
            .get_current_frame()
            .expect("Failed to acquire next swap chain texture")
            .output;

          let command_buffer = {
            let mut encoder = renderer
              .device
              .create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });
            {
              let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                  attachment: &current_frame.view,
                  resolve_target: None,
                  ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(if cfg!(debug_assertions) {
                      #[cfg_attr(rustfmt, rustfmt_skip)]
                      wgpu::Color { r: 0.8, g: 0.8, b: 0.8, a: 0.0 }
                    } else {
                      #[cfg_attr(rustfmt, rustfmt_skip)]
                      wgpu::Color { r: 0.0, g: 0.0, b: 0.0, a: 1.0 }
                    }),
                    store: true
                  }
                }],
                depth_stencil_attachment: None
              });
              rpass.set_pipeline(&renderer.render_pipeline);
              rpass.set_bind_group(
                0,
                if State::Finished != renderer.state {
                  &renderer.bind_group
                } else {
                  &renderer.end_bind_group
                },
                &[]
              );
              rpass.set_index_buffer(renderer.index_buffer.slice(..));
              rpass.set_vertex_buffer(0, renderer.vertex_buffer.slice(..));
              rpass.draw_indexed(0..4, 0, 0..1);
            }

            encoder.finish()
          };

          let command_buffer_sub = {
            let mut encoder = renderer
              .device
              .create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });

            {
              // renderer.elapsed_frames.clone_from(&renderer.current_frame);
              // renderer.elapsed_frames -= &renderer.first_frame;
              // renderer.elapsed_years.clone_from(&renderer.elapsed_frames);
              renderer.elapsed_years.clone_from(&renderer.current_frame);
              let mut divisor = (*FRAMES_FOR_YEAR).clone();
              renderer.elapsed_years.div_rem_mut(&mut divisor);
              renderer.elapsed_days.clone_from(&divisor);
              let mut divisor = (*FRAMES_FOR_DAY).clone();
              renderer.elapsed_days.div_rem_mut(&mut divisor);
              renderer.elapsed_hours.clone_from(&divisor);
              let mut divisor = (*FRAMES_FOR_HOUR).clone();
              renderer.elapsed_hours.div_rem_mut(&mut divisor);
              renderer.elapsed_mins.clone_from(&divisor);
              let mut divisor = (*FRAMES_FOR_10_MINUTES).clone();
              renderer.elapsed_mins *= 10;
              renderer.elapsed_mins.div_rem_mut(&mut divisor);
              renderer.elapsed_secs.clone_from(&renderer.fps);
              renderer.elapsed_secs.div_from(&divisor);
              renderer.elapsed_secs /= 10;

              {
                let years = Float::with_val(64, &renderer.elapsed_years);
                let days = renderer.elapsed_days.to_string();
                let hours = renderer.elapsed_hours.to_string();
                let minutes = renderer.elapsed_mins.to_string();
                let seconds = renderer.elapsed_secs.to_f32();
                let string_years = {
                  if years.is_zero() {
                    format!("0.00000000 × 10⁰ years|")
                  } else {
                    lazy_static! {
                      static ref RE: regex::Regex =
                        regex::Regex::new(r"([0-9.]+)(?:e([0-9]+))?").unwrap();
                    }
                    let pre = format!("{:.8}", years);
                    let caps = RE.captures(&pre).unwrap();
                    let mantissa = caps.get(1).unwrap();
                    if let Some(exponent) = caps.get(2) {
                      format!(
                        "{} × 10{} years|",
                        mantissa.as_str(),
                        to_sup(exponent.as_str())
                      )
                    } else {
                      let m = mantissa.as_str().parse::<f64>().unwrap();
                      let e = m.log10().floor() as i32;
                      let m = m / 10.0f64.powi(e);
                      format!("{:.8} × 10{} years|", m, to_sup(&e.to_string()))
                    }
                  }
                };
                let string_days = format!("{:} days |", days);
                let string_hours = format!("{:} hours|", hours);
                let string_minutes = format!("{:} mins |", minutes);
                let string_seconds = format!("{:.8} secs |", seconds);
                let mut output = Vec::new();
                let (line_height, line_width1, output1) = renderer.glyph_caches.populate(
                  &renderer.device,
                  &renderer.queue,
                  &string_years,
                  &renderer.storage_buffer,
                  &renderer.sampler
                );
                let (line_height, line_width2, output2) = renderer.glyph_caches.populate(
                  &renderer.device,
                  &renderer.queue,
                  &string_days,
                  &renderer.storage_buffer,
                  &renderer.sampler
                );
                let (line_height, line_width3, output3) = renderer.glyph_caches.populate(
                  &renderer.device,
                  &renderer.queue,
                  &string_hours,
                  &renderer.storage_buffer,
                  &renderer.sampler
                );
                let (line_height, line_width4, output4) = renderer.glyph_caches.populate(
                  &renderer.device,
                  &renderer.queue,
                  &string_minutes,
                  &renderer.storage_buffer,
                  &renderer.sampler
                );
                let (line_height, line_width5, output5) = renderer.glyph_caches.populate(
                  &renderer.device,
                  &renderer.queue,
                  &string_seconds,
                  &renderer.storage_buffer,
                  &renderer.sampler
                );

                let vp_width = renderer.storage_data[0].viewport[0];

                let offset = renderer.glyph_caches.advance_width;
                for mut glyph in output1 {
                  glyph.x += (vp_width / 2.0) - line_width1 + offset;
                  glyph.y += line_height * 0.0;
                  if glyph.key.c == '⁴' {
                    glyph.y -= 5.0;
                    glyph.height += 3usize;
                  }
                  output.push(glyph);
                }
                for mut glyph in output2 {
                  glyph.x += (vp_width / 2.0) - line_width2 + offset;
                  glyph.y += line_height * 1.0;
                  output.push(glyph);
                }
                for mut glyph in output3 {
                  glyph.x += (vp_width / 2.0) - line_width3 + offset;
                  glyph.y += line_height * 2.0;
                  output.push(glyph);
                }
                for mut glyph in output4 {
                  glyph.x += (vp_width / 2.0) - line_width4 + offset;
                  glyph.y += line_height * 3.0;
                  output.push(glyph);
                }
                for mut glyph in output5 {
                  glyph.x += (vp_width / 2.0) - line_width5 + offset;
                  glyph.y += line_height * 4.0;
                  output.push(glyph);
                }

                for (i, glyph) in output.iter().enumerate() {
                  renderer.storage_data[i].position[0] = glyph.x;
                  renderer.storage_data[i].position[1] = glyph.y;
                  renderer.storage_data[i].size[0] = glyph.width as f32;
                  renderer.storage_data[i].size[1] = glyph.height as f32;
                }
                renderer.queue.write_buffer(
                  &renderer.storage_buffer,
                  0,
                  bytemuck::cast_slice(&[renderer.storage_data])
                );

                let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                  color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                    attachment: &current_frame_sub.view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                      load: wgpu::LoadOp::Clear(if cfg!(debug_assertions) {
                        #[cfg_attr(rustfmt, rustfmt_skip)]
                            wgpu::Color { r: 0.8, g: 0.8, b: 0.8, a: 0.0 }
                      } else {
                        #[cfg_attr(rustfmt, rustfmt_skip)]
                            wgpu::Color { r: 0.0, g: 0.0, b: 0.0, a: 1.0 }
                      }),
                      store: true
                    }
                  }],
                  depth_stencil_attachment: None
                });
                rpass.set_pipeline(&renderer.sub_render_pipeline);
                rpass.set_index_buffer(renderer.index_buffer.slice(..));
                rpass.set_vertex_buffer(0, renderer.vertex_buffer.slice(..));

                for (i, glyph) in output.iter().enumerate() {
                  rpass.set_bind_group(0, &renderer.glyph_caches.hashmap[&glyph.key.c], &[]);
                  rpass.draw_indexed(0..4, 0, (i as u32)..(i as u32) + 1);
                }
              }
            }

            encoder.finish()
          };

          renderer
            .queue
            .submit(vec![command_buffer, command_buffer_sub]);
        }
      }
      _ => {}
    }
  });
}

const DEFAULT_SIZE: winit::dpi::Size =
  winit::dpi::Size::Physical(winit::dpi::PhysicalSize::new(720, 480));
const DEFAULT_POSITION: winit::dpi::Position =
  winit::dpi::Position::Physical(winit::dpi::PhysicalPosition::new(1440, 480));
const DEFAULT_SUB_POSITION: winit::dpi::Position =
  winit::dpi::Position::Physical(winit::dpi::PhysicalPosition::new(680, 480));

fn main() {
  let event_loop = EventLoop::new();
  let window = winit::window::WindowBuilder::new()
    .with_min_inner_size(DEFAULT_SIZE)
    .with_inner_size(DEFAULT_SIZE)
    .with_title(format!("v{}", VERSION).to_string())
    .build(&event_loop)
    .unwrap();
  //let subevent_loop = EventLoop::new();
  let subwindow = winit::window::WindowBuilder::new()
    .with_min_inner_size(DEFAULT_SIZE)
    .with_inner_size(DEFAULT_SIZE)
    .with_title(format!("v{}", VERSION).to_string())
    .build(&event_loop)
    .unwrap();
  window.set_outer_position(DEFAULT_POSITION);
  subwindow.set_outer_position(DEFAULT_SUB_POSITION);
  wgpu_subscriber::initialize_default_subscriber(None);
  futures::executor::block_on(run(
    event_loop,
    window,
    wgpu::TextureFormat::Bgra8UnormSrgb,
    subwindow
  ));
}