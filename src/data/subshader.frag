#version 450

layout(location = 0) in vec2 v_uv;
layout(location = 1) flat in uint instance_index;
layout(location = 0) out vec4 target0;

layout(set = 0, binding = 1) uniform texture2D u_texture;
layout(set = 0, binding = 2) uniform sampler u_sampler;

void main() {
  float coverage = texture(sampler2D(u_texture, u_sampler), v_uv).r;
  target0.r = 1.0;
  target0.g = 1.0;
  target0.b = 1.0;
  target0.a = coverage;
}