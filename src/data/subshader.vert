#version 450

layout(location = 0) in vec2 a_pos;
layout(location = 1) in vec2 a_uv;
layout(location = 0) out vec2 v_uv;
layout(location = 1) out uint instance_index;

struct StorageBufferData {
  vec2 viewport;
  vec2 position;
  vec2 size;
  uint is_scaled;
  uint dummy_0;
};

layout(set = 0, binding = 0) buffer StorageBuffer {
  StorageBufferData storage_buffer[];
};

out gl_PerVertex {
  vec4 gl_Position;
};

const vec2 original_vp = vec2(720.0, 480.0);
const float desired_aspect_ratio = 720.0 / 480.0;

void main() {
  vec2 vp = storage_buffer[0].viewport;
  vec2 desired_size =
    storage_buffer[0].is_scaled == 1
    ? (vp.y * desired_aspect_ratio <= vp.x)
      ? vec2(vp.y * desired_aspect_ratio, vp.y)
      : vec2(vp.x, vp.x / desired_aspect_ratio) // FIXME
    : storage_buffer[gl_InstanceIndex].size;

  vec2 transformer = desired_size / vp;
  float x = storage_buffer[gl_InstanceIndex].position.x * 2.0;
  float y = storage_buffer[gl_InstanceIndex].position.y * 2.0;
  float width = desired_size.x;
  float height = desired_size.y;
  vec2 test = vec2(x, y + height) / vp;
  vec2 result = a_pos * transformer + test;

  gl_Position = vec4(result, 0.0, 1.0);
  v_uv = a_uv;
  instance_index = gl_InstanceIndex;
}