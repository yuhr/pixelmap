#version 450

layout(location = 0) in vec2 a_pos;
layout(location = 1) in vec2 a_uv;
layout(location = 0) out vec2 v_uv;

layout(set = 0, binding = 0) uniform UniformBuffer {
  vec2 viewport;
  uint is_scaled;
} uniform_buffer;

out gl_PerVertex {
  vec4 gl_Position;
};

const vec2 original_vp = vec2(720.0, 480.0);
const float desired_aspect_ratio = 720.0 / 480.0;

void main() {
  vec2 vp = uniform_buffer.viewport;
  vec2 desired_size =
    uniform_buffer.is_scaled == 1
    ? (vp.y * desired_aspect_ratio <= vp.x)
      ? vec2(vp.y * desired_aspect_ratio, vp.y)
      : vec2(vp.x, vp.x / desired_aspect_ratio)
    : original_vp;

  vec2 filled = a_pos;
  vec2 transformer = desired_size / vp;
  vec2 result = filled * transformer;

  gl_Position = vec4(result, 0.0, 1.0);
  v_uv = a_uv;
}