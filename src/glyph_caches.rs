use crate::glyph::Glyph;
use std::collections::HashMap;

pub struct GlyphCaches {
  pub hashmap: HashMap<char, wgpu::BindGroup>,
  font: fontdue::Font,
  sub_bind_group_layout: wgpu::BindGroupLayout,
  pub advance_width: f32
}

impl GlyphCaches {
  pub fn new(font: fontdue::Font, sub_bind_group_layout: wgpu::BindGroupLayout) -> GlyphCaches {
    let metrics = font.metrics(' ', 32.0);
    let advance_width = metrics.advance_width;
    GlyphCaches {
      hashmap: HashMap::new(),
      font,
      sub_bind_group_layout,
      advance_width
    }
  }
  pub fn populate(
    &mut self,
    device: &wgpu::Device,
    queue: &wgpu::Queue,
    string: &str,
    storage_buffer: &wgpu::Buffer,
    sampler: &wgpu::Sampler
  ) -> (f32, f32, Vec<fontdue::layout::GlyphPosition>) {
    let fonts = &[&self.font];
    let styles = &[&fontdue::layout::TextStyle::new(string, 32.0, 0)];
    let mut output = Vec::new();
    let mut layout = fontdue::layout::Layout::new();
    let settings = fontdue::layout::LayoutSettings {
      // max_width: Some(720.0),
      // horizontal_align: fontdue::layout::HorizontalAlign::Right,
      //include_whitespace: true,
      ..fontdue::layout::LayoutSettings::default()
    };
    layout.layout_horizontal(fonts, styles, &settings, &mut output);
    let line_height = self.font.vertical_line_metrics(32.0).unwrap().new_line_size;

    for glyph in &output {
      use std::collections::hash_map::Entry;
      if let Entry::Vacant(hashmap) = self.hashmap.entry(glyph.key.c) {
        let (metrics, bitmap) = self.font.rasterize_config(glyph.key);
        let texture = Glyph::new(device, glyph.width as u32, glyph.height as u32);
        texture.write(&queue, &bitmap);
        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
          layout: &self.sub_bind_group_layout,
          entries: &[
            wgpu::BindGroupEntry {
              binding: 0,
              resource: wgpu::BindingResource::Buffer(storage_buffer.slice(..))
            },
            wgpu::BindGroupEntry {
              binding: 1,
              resource: wgpu::BindingResource::TextureView(&texture.view)
            },
            wgpu::BindGroupEntry {
              binding: 2,
              resource: wgpu::BindingResource::Sampler(sampler)
            }
          ],
          label: None
        });
        hashmap.insert(bind_group);
      }
    }

    let last = &output.last().unwrap();
    let entire_width = last.x + last.width as f32;
    (line_height, entire_width, output)
  }
}